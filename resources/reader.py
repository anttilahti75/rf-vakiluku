def read_data(arg1):
    data = []
    with open(arg1, newline='') as f:
        i = list(f)
        for item in i:
            item = item.rstrip()
            item = item.split(";")
            data.append(item)
    return data

def write_csv(arg1,country,city,pop):
    with open(arg1, "a") as file:
        file.write(country + ";" + city + ";" + pop + "\n")