*** Settings ***
Library     SeleniumLibrary
Library     ../resources/reader.py
Library     ../resources/excel.py

*** Variables ***
${URL}          https://countrymeters.info/en/
${BROWSER}      headlesschrome
${CHROME_OPTIONS}   add_argument("--no-sandbox"); add_argument("--disable-dev-shm-usage")
#${BROWSER}      chrome
${COUNTRY}      later
${FILE}         ./resources/kaupungit.csv
${FILE2}         ./resources/kaupungit2.csv
${EXCEL_FILE_IN}   ./resources/kaupungit.xlsx
${EXCEL_FILE_OUT}   ${CURDIR}/n7540_kaupungit.xlsx
${EXCEL_INDEX}      1
${OUTLOOK_URL}     https://outlook.live.com/owa/?nlp=1
${ONEDRIVE_URL}     https://onedrive.live.com
${USER}         USERNAME@outlook.com
${PASS}         PASSWORD

*** Keywords ***
Browser  
        Open Browser        ${URL}      ${BROWSER}      options=${CHROME_OPTIONS}

Get Country
###
# Excel-file
###

    ${excel_lista}=     from_excel      ${EXCEL_FILE_IN}
    FOR     ${item}     IN      @{excel_lista}
        #Log To Console      ${item}
        #Log To Console      ${item[0]}
        #Log To Console      ${item[1]}
        Go To       ${URL}${item[0]}
        ${value}    Get Text    id:cp1
        #Log To Console      ${value}
        to_excel    ${EXCEL_FILE_OUT}   ${EXCEL_INDEX}      ${item[0]}    ${item[1]}   ${value}
        ${EXCEL_INDEX}=   Evaluate    ${EXCEL_INDEX} + 1
    END

###
# CSV-file
###
#    ${lista}=   read_data   ${FILE}
#    FOR     ${item}     IN      @{excel_lista}
#        #Log To Console      ${item}
#        #Log To Console      ${item[0]}
#        #Log To Console      ${item[1]}
#        Go To       ${URL}${item[0]}
#        ${value}    Get Text    id:cp1
#        #Log To Console      ${value}
#        write_csv   ${FILE2}     ${item[0]}    ${item[1]}   ${value}
#    END

Upload Excel
    Go To       ${OUTLOOK_URL}Sleep       60
    Wait Until Page Contains Element       name:loginfmt
    Input Text      name:loginfmt     ${USER}
    Click Element   id:idSIButton9
    Wait Until Page Contains Element       name:passwd
    Input Text      name:passwd    ${PASS}
    Click Element   id:idSIButton9
    Go To       ${ONEDRIVE_URL}
#    Click Element   name:Upload
    Choose File     class:commandFileInput     ${EXCEL_FILE_OUT}
#    Drag And Drop   xpath://html/body/div[1]/div[1]/div/div[2]     /home/lahan/PycharmProjects/robot_framework/vakiluku/code/resources/kaupungit.xlsx
#    Click Element   name:Files

    #Close All Browsers
