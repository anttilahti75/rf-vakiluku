from openpyxl import Workbook
from openpyxl import load_workbook

wb = Workbook()

#ws['A1'] = "country"
#ws['B1'] = "city"
#ws['C1'] = "population"

def from_excel(arg1):
    wb = load_workbook(filename = arg1)
    ws = wb.active
    data = []
    i = ws.values
    for item in i:
        data.append(item)
    return data

def to_excel(arg1,arg2,arg3,arg4,arg5):
    ws = wb.active
    arg2 = str(arg2)
    co = "A" + arg2
    ci = "B" + arg2
    po = "C" + arg2
    ws[co] = arg3
    ws[ci] = arg4
    ws[po] = arg5
    wb.save(arg1)