# Intro
- This robot framework test case fetches population count for different countries from a website and saves it in a spreadsheet file. Target countries are in an another spreadsheet file.

# How to use
- Assuming you have docker ready, clone this repo and run `docker build -t name .`
- If you do not want to specify the resulting image name, omit the "-t name" part