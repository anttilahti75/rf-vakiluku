FROM ubuntu:19.10

WORKDIR /root/

RUN apt-get update && \
    apt-get -y install python3 python3-pip xvfb curl wget unzip

RUN pip3 install robotframework-seleniumlibrary openpyxl

RUN wget -O linux_signing_key.pub https://dl-ssl.google.com/linux/linux_signing_key.pub && \
    apt-key add linux_signing_key.pub && \
    echo "deb [arch=amd64]  http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list

RUN apt-get -y update && \
    apt-get -y install google-chrome-stable

RUN wget https://chromedriver.storage.googleapis.com/2.41/chromedriver_linux64.zip && \
    unzip chromedriver_linux64.zip && \
    mv chromedriver /usr/bin/chromedriver && \
    chown root:root /usr/bin/chromedriver && \
    chmod +x /usr/bin/chromedriver

COPY ./resources/ /root/code/resources/
COPY ./tasks/ /root/code/tasks

WORKDIR /root/code/

ENTRYPOINT ["robot", "-d", "output/", "tasks/main.robot"]